<?php


class Receiver
{
    public function doA()
    {
        echo 'Receiver doing A' . PHP_EOL;
    }

    public function doB()
    {
        echo 'Receiver doing B' . PHP_EOL;
    }
} 